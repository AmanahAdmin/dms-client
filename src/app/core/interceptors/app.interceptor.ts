/**
 * app interceptor
 *
 *
 *  @author Mustafa Omran <promustafaomran@hotmail.com>
 *
 */

import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpEvent,
    HttpRequest,
    HttpHandler,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()

export class AppInterceptor implements HttpInterceptor {

    constructor() { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // if (localStorage.getItem('user') !== null) {
        //     const token = JSON.parse(localStorage.getItem('user'));            
        //     request = request.clone({ headers: request.headers.set('Authorization', `Bearer ${token.user.jwt}`) });
        // }

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                }
                if (event instanceof HttpErrorResponse) {
                }

                return event;
            }));


    }

}
