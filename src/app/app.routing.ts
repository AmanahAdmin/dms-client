import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('./dashboard/pages/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'teams',
        loadChildren: () => import('./dashboard/pages/teams/teams.module').then(m => m.TeamsModule)
      },
      {
        path: 'access-control',
        loadChildren: () => import('./dashboard/pages/access-control/access-control.module').then(m => m.AccessControlModule)
      },
      {
        path: 'auto-allocation',
        loadChildren: () => import('./dashboard/pages/auto-allocation/auto-allocation.module').then(m => m.AutoAllocationModule)
      },
      {
        path: 'geo-fence',
        loadChildren: () => import('./dashboard/pages/geo-fence/geo-fence.module').then(m => m.GeoFenceModule)
      },
      {
        path: 'manager',
        loadChildren: () => import('./dashboard/pages/manager/manager.module').then(m => m.ManagerModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./dashboard/pages/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'logs',
        loadChildren: () => import('./dashboard/pages/account-logs/account-logs.module').then(m => m.AccountLogsModule)
      }
    ]
  }
];
