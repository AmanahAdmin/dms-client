import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGeoFenceComponent } from './add-geo-fence.component';

describe('AddGeoFenceComponent', () => {
  let component: AddGeoFenceComponent;
  let fixture: ComponentFixture<AddGeoFenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGeoFenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGeoFenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
