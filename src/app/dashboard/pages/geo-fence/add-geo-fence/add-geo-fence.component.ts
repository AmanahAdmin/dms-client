import { Component, OnInit } from '@angular/core';
import * as Leaflet from 'leaflet';

interface Team {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-add-geo-fence',
  templateUrl: './add-geo-fence.component.html',
  styleUrls: ['./add-geo-fence.component.scss']
})
export class AddGeoFenceComponent implements OnInit {
  map: any;
  teams: Team[] = [
    { value: 'value-1', viewValue: 'Value 1' },
    { value: 'value-2', viewValue: 'Value 2' },
    { value: 'value-3', viewValue: 'Value 3' }
  ];

  entireTeam: boolean = false;
  agents: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];

  constructor() {
  }

  ngOnInit() {
    this.initMap();
  }

  /**
   * init leaflet map
   * 
   * 
   */
  private initMap(): void {
    this.map = Leaflet.map('map', {
      center: [39.8282, -98.5795],
      zoom: 3
    });

    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
    });
    tiles.addTo(this.map);

  }

  cancel() {

  }
}
