import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeoFenceComponent } from './geo-fence.component';
import { AddGeoFenceComponent } from './add-geo-fence/add-geo-fence.component';
const routes: Routes = [
  {
    path: '',
    component: GeoFenceComponent,
  },
  {
    path: 'add',
    component: AddGeoFenceComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeoFenceRoutingModule { }
