import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeoFenceRoutingModule } from './geo-fence-routing.module';
import { GeoFenceComponent } from './geo-fence.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';

import { AddGeoFenceComponent } from './add-geo-fence/add-geo-fence.component';

@NgModule({
  declarations: [GeoFenceComponent, AddGeoFenceComponent],
  imports: [
    CommonModule,
    GeoFenceRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    // material
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatListModule
  ]
})
export class GeoFenceModule { }
