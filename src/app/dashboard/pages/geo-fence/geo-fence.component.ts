import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Leaflet from 'leaflet';
interface Team {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-geo-fence',
  templateUrl: './geo-fence.component.html',
  styleUrls: ['./geo-fence.component.scss']
})
export class GeoFenceComponent implements OnInit {

  map: any;

  teams: Team[] = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' }
  ];

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.initMap();
  }

  private initMap(): void {
    this.map = Leaflet.map('map', {
      center: [39.8282, -98.5795],
      zoom: 3
    });

    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
    });

    tiles.addTo(this.map);
  }


  /**
   * navigate to add geo fence page
   * 
   * 
   */
  createGeoFence() {
    this.router.navigate(['geo-fence/add']);
  }
}