import { Component, OnInit } from '@angular/core';

export interface Row {
  events: string;
  email: boolean;
  sms: boolean;
  webhooks: boolean;
}

const EVENTS: Row[] = [
  { events: 'Hydrogen', email: false, sms: true, webhooks: true },
  { events: 'Helium', email: false, sms: false, webhooks: true },
  { events: 'Lithium', email: false, sms: false, webhooks: true },
  { events: 'Beryllium', email: false, sms: false, webhooks: true },
  { events: 'Boron', email: false, sms: false, webhooks: true },
  { events: 'Carbon', email: true, sms: false, webhooks: false },
];


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  displayedColumns: string[] = ['events', 'sms', 'email', 'webhooks', 'actions'];
  pickupDataSource = EVENTS;

  page: number = 10;

  constructor() {

  }

  ngOnInit() {

  }

  onConfirm(event) {
  }
}
