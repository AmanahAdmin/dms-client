import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-basic-user-info',
  templateUrl: './basic-user-info.component.html',
  styleUrls: ['./basic-user-info.component.scss']
})
export class BasicUserInfoComponent implements OnInit {

  selectedType: string;
  types: any = [
    {
      value: 'agent',
      name: `AGENT'S IMAGE`,
    },
    {
      value: 'company',
      name: `Company's Logo`,
    }
  ];

  form: FormGroup;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      firstName: [''],
      lastName: [''],
      phone: [''],
      email: [''],
      companyName: [''],
      companyAddress: [''],
      country: [''],
      userId: [''],
      agentText: [''],
      imageType: ['']
    });
  }

  ngOnInit() {
  }

}
