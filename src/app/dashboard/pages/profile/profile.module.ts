import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// components
import { ProfileComponent } from './profile.component';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { ChanagePasswordComponent } from './chanage-password/chanage-password.component';
import { BasicUserInfoComponent } from './basic-user-info/basic-user-info.component';

import {
  MatAutocompleteModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatRadioModule,
} from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";

import { ChangeLanguageComponent } from './change-language/change-language.component';

@NgModule({
  declarations: [
    ProfileComponent,
    DeleteAccountComponent,
    ChanagePasswordComponent,
    BasicUserInfoComponent,
    ChangeLanguageComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatAutocompleteModule,
    MatIconModule,
    MatRadioModule,
    MatInputModule,
    FlexLayoutModule,
  ]
})
export class ProfileModule { }
