import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.scss']
})
export class DeleteAccountComponent implements OnInit {

  // delete section
  selectedReason: string;
  seasons: string[] = [
    'I’m lost and need help with my account',
    'I need more time to test',
    'I need to pause my billing',
    'I want to start fresh later',
    'I just want to delete my account',
    'It costs too much',
    'I found another app that I like better'
  ];


  constructor() { }

  ngOnInit() {
  }

}
