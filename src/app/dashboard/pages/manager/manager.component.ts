import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ManageManagerComponent } from './manage-manager/manage-manager.component';
import { MatTableDataSource } from '@angular/material/table';

export interface Row {
  id: number;
  name: string;
  email: string;
  actions?: string;
  phone: string;
  teams: string;
  role: string;
}

const ELEMENT_DATA: Row[] = [
  { id: 1, name: 'Hydrogen', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 2, name: 'Helium', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 3, name: 'Lithium', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 4, name: 'Beryllium', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 5, name: 'Boron', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 6, name: 'Carbon', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 7, name: 'Nitrogen', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 8, name: 'Oxygen', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 9, name: 'Fluorine', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
  { id: 10, name: 'Neon', email: 'username@domain.com', phone: '012**********', teams: 'team_x, team_y', role: 'role type' },
];

export interface DialogData {

}

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent {

  displayedColumns: string[] = ['id', 'name', 'email', 'phone', 'teams', 'role', 'actions'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  dialogContent: Object;
  page: number = 10;

  constructor(public dialog: MatDialog) {
    this.dialogContent = {
      title: "Confirm to delete ?",
      message: "Are you sure to delete this role",
      openBtn: "Delete",
      cancelBtn: "Cancel",
      okayBtn: "Okay, yes!",
    }
  }

  /**
   * manage role dialog
   * 
   * 
   */
  manageManagerDialog(): void {
    const dialogRef = this.dialog.open(ManageManagerComponent, {
      width: '750px',
      minHeight: '350px',
      data: {}
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);

      if (result) {
        // ...
      } else {
        dialogRef.close();
      }
    });
  }

  onConfirm(event) {


  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
