import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-manage-manager',
  templateUrl: './manage-manager.component.html',
  styleUrls: ['./manage-manager.component.scss']
})
export class ManageManagerComponent implements OnInit {

  types: Type[] = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' }
  ];

  type: string = 'password';
  panelOpenState = false;

  constructor(public dialogRef: MatDialogRef<ManageManagerComponent>) { }

  ngOnInit() {

  }


  /**
   * close dialog
   * 
   * 
   */
  cancel(): void {
    this.dialogRef.close();
  }

  /**
   * password to text and inverse
   * 
   * 
   */
  togglePassword() {
    if (this.type == 'password') {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
