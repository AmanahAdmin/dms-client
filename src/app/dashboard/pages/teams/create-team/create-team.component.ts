import { Component, Inject } from '@angular/core';
import { DialogData } from './../teams.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';

export interface Tag {
  name: string;
}

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent {

  constructor(
    public dialogRef: MatDialogRef<CreateTeamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }


  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  tags: Tag[] = [{ name: 'ex, myTag' }];

  types: any[] = [
    { value: 'high', viewValue: 'High' },
    { value: 'medium', viewValue: 'Medium' },
    { value: 'low', viewValue: 'Low' },
    { value: 'no-tracking', viewValue: 'No Tracking' },
  ];

  /**
   * add new tag
   * 
   * 
   * @param event MatChipInputEvent
   */
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push({ name: value.trim() });
    }

    if (input) {
      input.value = '';
    }
  }

  /**
   * remove tag
   * 
   * 
   * @param tag 
   */
  remove(tag: Tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  /**
   * close dialog
   * 
   * 
   */
  cancel() {
    this.dialogRef.close();
  }
}
