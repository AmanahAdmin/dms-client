import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateTeamComponent } from './create-team/create-team.component';

export interface DialogData {
  name: string;
}


@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent {

  name: string;
  dialogContent: Object;
  page: number = 10;

  constructor(public dialog: MatDialog) {
    this.dialogContent = {
      title: "Confirm to delete ?",
      message: "Are you sure to delete this item",
      openBtn: "Delete",
      cancelBtn: "Cancel",
      okayBtn: "Okay, yes!",
    }
  }


  /**
   * create team dialog
   * 
   * 
   */
  openCreateTeamDialog(): void {
    const dialogRef = this.dialog.open(CreateTeamComponent, {
      width: '500px',
      minHeight: '450px',
      data: {}
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // ...
      } else {
        // ...
      }
    });
  }

  onConfirm(event) {
    console.log('event', event);
  }
}