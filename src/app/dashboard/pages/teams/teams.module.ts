import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamsRoutingModule } from './teams-routing.module';
import { TeamsComponent } from './teams.component';
import { CreateTeamComponent } from './create-team/create-team.component';

// angular material
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';

// shared
import { ConfirmDeletionModule } from '@dms/app/shared/confirm-deletion/confirm-deletion.module';

// third party
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    TeamsComponent,
    CreateTeamComponent
  ],
  imports: [
    CommonModule,

    // material
    MatSelectModule,
    TeamsRoutingModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatChipsModule,
    MatCardModule,
    FlexLayoutModule,
    MatMenuModule,
    MatPaginatorModule,
    MatTooltipModule,

    // shared
    ConfirmDeletionModule,

    // third party
    NgxPaginationModule,
  ],
  entryComponents: [
    CreateTeamComponent
  ]
})
export class TeamsModule { }
