import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange, MatButtonToggleChange } from '@angular/material';
import { SnackBar, Body } from '@dms/utilities/snakbar';

@Component({
  selector: 'app-auto-allocation',
  templateUrl: './auto-allocation.component.html',
  styleUrls: ['./auto-allocation.component.scss'],
})

export class AutoAllocationComponent implements OnInit {

  group: any;
  isDisabledOne: boolean = true;
  isDisabledTwo: boolean = true;
  selectedMehtod: string = '';

  constructor(private snackBar: SnackBar) {
  }

  ngOnInit() {
  }

  /**
   * Disable / Enable methods
   * 
   * 
   * @param event MatSlideToggleChange
   */
  onChange(event: MatSlideToggleChange) {
    const value = event.checked;

    this.isDisabledOne = !value;
    this.isDisabledTwo = !value;
    this.selectedMehtod = '';

    if (value) {
      const body: Body = {
        message: "Auto allocations has been enabled",
        action: "Okay",
        duration: 2000
      }

      this.snackBar.openSnackBar(body);
    } else {
      const body: Body = {
        message: "Auto allocations has been disabled",
        action: "Okay",
        duration: 2000
      }

      this.snackBar.openSnackBar(body);
    }
  }

  /**
   * change method
   * 
   * 
   * @param event MatButtonToggleChange
   */
  onChangeMethod(event: MatButtonToggleChange) {
    const value = event.value;
    this.selectedMehtod = value;
  }
}
