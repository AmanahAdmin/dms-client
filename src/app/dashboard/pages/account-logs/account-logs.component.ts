import { Component, OnInit } from '@angular/core';

export interface Row {
  id: number;
  user: string;
  date: string;
  description: string;
  activityType: string;
}

const ELEMENT_DATA: Row[] = [
  { id: 1, user: 'Hydrogen', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 2, user: 'Helium', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 3, user: 'Lithium', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 4, user: 'Beryllium', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 5, user: 'Boron', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 6, user: 'Carbon', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 7, user: 'Nitrogen', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 8, user: 'Oxygen', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 9, user: 'Fluorine', description: '012**********', activityType: 'type of activity', date: 'date - time' },
  { id: 10, user: 'Neon', description: '012**********', activityType: 'type of activity', date: 'date - time' },
];


@Component({
  selector: 'app-account-logs',
  templateUrl: './account-logs.component.html',
  styleUrls: ['./account-logs.component.scss']
})
export class AccountLogsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'user', 'description', 'activityType', 'date'];
  dataSource = ELEMENT_DATA;

  page: number = 10;

  constructor() {

  }

  ngOnInit() {

  }

}
