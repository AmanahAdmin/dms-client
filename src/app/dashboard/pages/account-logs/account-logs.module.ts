import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountLogsRoutingModule } from './account-logs-routing.module';
import { AccountLogsComponent } from './account-logs.component';

// angular material
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [AccountLogsComponent],
  imports: [
    CommonModule,
    AccountLogsRoutingModule,

    //material
    MatIconModule,
    MatTableModule,
    MatMenuModule,
    MatButtonModule,
    FlexLayoutModule,
    MatPaginatorModule
  ]
})
export class AccountLogsModule { }
