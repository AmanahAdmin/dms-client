import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ManageRoleComponent } from './manage-role/manage-role.component';

export interface PeriodicElement {
  id: number;
  name: string;
  createdAt: string;
  actions?: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { id: 1, name: 'Hydrogen', createdAt: '01/12/2020' },
  { id: 2, name: 'Helium', createdAt: '02/03/2020' },
  { id: 3, name: 'Lithium', createdAt: '02/03/2020' },
  { id: 4, name: 'Beryllium', createdAt: '01/21/2020' },
  { id: 5, name: 'Boron', createdAt: '02/03/2020' },
  { id: 6, name: 'Carbon', createdAt: '01/06/2020' },
  { id: 7, name: 'Nitrogen', createdAt: '01/21/2020' },
  { id: 8, name: 'Oxygen', createdAt: '02/03/2020' },
  { id: 9, name: 'Fluorine', createdAt: '01/06/2020' },
  { id: 10, name: 'Neon', createdAt: '01/11/2020' },
];

export interface DialogData {

}


@Component({
  selector: 'app-access-control',
  templateUrl: './access-control.component.html',
  styleUrls: ['./access-control.component.scss']
})
export class AccessControlComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'createdAt', 'actions'];
  dataSource = ELEMENT_DATA;

  dialogContent: Object;
  page: number = 10;

  constructor(public dialog: MatDialog) {
    this.dialogContent = {
      title: "Confirm to delete ?",
      message: "Are you sure to delete this role",
      openBtn: "Delete",
      cancelBtn: "Cancel",
      okayBtn: "Okay, yes!",
    }
  }

  ngOnInit() {
  }


  /**
   * manage role dialog
   * 
   * 
   */
  openRoleDialog(): void {
    const dialogRef = this.dialog.open(ManageRoleComponent, {
      width: '750px',
      minHeight: '350px',
      data: {}
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);

      if (result) {
        // ...
      } else {
        dialogRef.close();
      }
    });
  }

  onConfirm(event) {

  }
}
