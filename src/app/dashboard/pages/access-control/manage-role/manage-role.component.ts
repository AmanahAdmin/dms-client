import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-manage-role',
  templateUrl: './manage-role.component.html',
  styleUrls: ['./manage-role.component.scss']
})
export class ManageRoleComponent {

  panelOpenState = false;
  constructor(public dialogRef: MatDialogRef<ManageRoleComponent>) { }

  cancel(): void {
    this.dialogRef.close();
  }

}
