import { Injectable, Injector } from '@angular/core';
import { LoginService } from './login.service';
import { ToExcelService } from './to-excel.service';

@Injectable({
  providedIn: 'root'
})

export class FacadeService {

  private _loginService: LoginService;
  private _toExcelService: ToExcelService;

  constructor(private inject: Injector) { }


  /**
   * login service
   * 
   * 
   */
  public get loginService(): LoginService {
    if (!this._loginService) {
      this._loginService = this.inject.get(LoginService)
    }

    return this._loginService;
  }

  /**
   * convert table excel service
   * 
   * 
   */
  public get toExcelService(): ToExcelService {
    if (!this._toExcelService) {
      this._toExcelService = this.inject.get(ToExcelService)
    }
    return this._toExcelService;
  }

}
