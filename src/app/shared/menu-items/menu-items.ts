import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  {
    state: 'profile',
    type: 'link',
    name: 'Profile',
    icon: 'view_list'
  },
  {
    state: 'teams',
    type: 'link',
    name: 'Teams',
    icon: 'people'
  },
  {
    state: 'access-control',
    type: 'link',
    name: 'Access Control',
    icon: 'accessibility'
  },
  {
    state: 'auto-allocation',
    type: 'link',
    name: 'Auto Allocation',
    icon: 'details'
  },
  {
    state: 'geo-fence',
    type: 'link',
    name: 'Geo Fence',
    icon: 'rounded_corner'
  },
  {
    state: 'manager',
    type: 'link',
    name: 'Manager',
    icon: 'important_devices'
  },
  {
    state: 'notifications',
    type: 'link',
    name: 'Notifications',
    icon: 'notification_important'
  },
  {
    state: 'logs',
    type: 'link',
    name: 'Logs',
    icon: 'history'
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
