import { Component, Input, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface Dialog {
  title: string;
  message: string;
  openBtn: string;
  cancelBtn: string;
  okayBtn: string;
}

@Component({
  selector: 'app-confirm-deletion',
  templateUrl: './confirm-deletion.component.html',
  styleUrls: ['./confirm-deletion.component.scss']
})
export class ConfirmDeletionComponent {

  @Input() dialogContent: Dialog;
  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public dialog: MatDialog) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverview, {
      minWidth: '300px',
      minHeight: '200px',
      data: this.dialogContent
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.confirm.emit(result);
      } else {
        this.confirm.emit(false);
      }
    });
  }
}



@Component({
  selector: 'app-dialog-overview',
  templateUrl: './dialog-overview.html',
  styleUrls: ['./confirm-deletion.component.scss']
})

export class DialogOverview {

  constructor(
    public dialogRef: MatDialogRef<DialogOverview>,
    @Inject(MAT_DIALOG_DATA) public data: Dialog) {
  }

  /**
   * cancel dialog
   * 
   * 
   */
  cancel(): void {
    this.dialogRef.close();
  }
}
