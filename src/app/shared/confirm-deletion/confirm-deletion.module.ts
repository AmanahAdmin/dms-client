import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeletionComponent } from './confirm-deletion.component';
import { DialogOverview } from './confirm-deletion.component';

// material
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    ConfirmDeletionComponent,
    DialogOverview
  ],
  imports: [
    CommonModule,

    // material
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    FlexLayoutModule
  ],
  exports: [
    ConfirmDeletionComponent,
  ],
  entryComponents: [
    DialogOverview
  ]
})
export class ConfirmDeletionModule { }
